package com.example.arkanoid

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    var heartsImages: Array<ImageView>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        gameView.resumeGame()


        heartsImages = arrayOf(imageView, imageView2, imageView3, imageView4, imageView5)
    }

    fun update() {
        Log.d("lastHope", "wykonuje")
        setScoreValue()
        if (heartsImages != null) {
            setHeartValue()
        }
    }

    private fun setScoreValue() {
        scoreValue.text = gameView.score.toString()
    }

    private fun setHeartValue() {

        val nr = gameView.lives
        for (i in (0..4)) {
            if (i + 1 <= nr) {
                heartsImages!![i].setImageResource(R.drawable.red)
            } else {
                heartsImages!![i].setImageResource(R.drawable.black)
            }
        }
    }

    override fun onPause() {
        gameView.backUpGame()
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
        //gameView.resumeGame()
    }

    override fun onDestroy() {
        gameView.restartGame()
        super.onDestroy()
    }

}
