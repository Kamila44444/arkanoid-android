package com.example.arkanoid

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.util.Log
import android.view.Gravity
import android.view.MotionEvent
import android.view.SurfaceHolder
import android.view.SurfaceView
import android.widget.Toast
import java.lang.Math.abs
import java.util.*
import kotlin.collections.ArrayList




class GameView(context: Context, attributeSet: AttributeSet)
    : SurfaceView(context, attributeSet), SurfaceHolder.Callback {

    private val thread : GameThread

    private var ballX = 30f
    private var ballY = -1f//todo
    private var dx = 0f
    private var dy = 0f
    private var ballSize = 30f
    private var maxScore = 0

    private var paddleX = 0f
    private var paddleSize = 150f
    private var paddleTop = height -30f
    private var targetList : ArrayList<Target>? = ArrayList<Target>()
    private var targetXNumber = 0
    private var targetYNumber = 0
    private var wasFirstDraw = false
    var score : Int = 0
    var lives : Int = 3



    init {
        holder.addCallback(this)
        thread = GameThread(holder, this, context as MainActivity)
        createTargetMap(5, 9)

    }

    private fun createTargetMap(targetXNumber : Int, targetYNumber : Int) {
        this.targetXNumber = targetXNumber
        this.targetYNumber = targetYNumber
        for(i in (1 .. targetXNumber)){
            for(j in (1 .. targetYNumber)){
                var strength = 1
                var material = "glass"
                if(j % 3 == 2) {
                    material = "brick"
                    strength = 2
                }
                if(j % 3 == 1){
                    material = "steel"
                    strength = 3
                }
                var drop : String = "empty"
                val gen = Random()
                if(gen.nextBoolean()){
                    drop = generateSurprise()
                }
                val tmpTarget = Target(i, j, 0F, 0F, 0F, 0F, strength, drop, material)
                targetList!!.add(tmpTarget)
            }
        }

    }


    private fun generateSurprise(): String {
        val gen = Random()
        when(gen.nextInt(14)){
            0 -> return "heart"
            1,6,10,11-> return "doubleSpeed"
            2,8 -> return "undoubleSpeed"
            3,9 -> return "doubleSize"
            4,7,12,13 -> return "undoubleSize"
        }
        return "empty"
    }


    override fun surfaceChanged(holder: SurfaceHolder?, format: Int, width: Int, height: Int) {
    }

    override fun surfaceDestroyed(holder: SurfaceHolder?) {
        thread.setRunning(false)
        thread.join()
    }

    override fun surfaceCreated(holder: SurfaceHolder?) {
        thread.setRunning(true)
        thread.start()
    }
    fun backUpGame(){
        Log.d("zapis", "ma dane? $ballY")
        val sharedPref = (context as MainActivity).getPreferences(Context.MODE_PRIVATE)
        val editor = sharedPref.edit()
        editor.putInt("lives", lives)
        editor.putInt("score", score)
        editor.putInt("number", targetList!!.size)
        for((i, t) in targetList!!.withIndex()){
            editor.putInt("target $i x", t.x)
            editor.putInt("target $i y", t.y)
            editor.putInt("target $i strength", t.strength)
            editor.putString("target $i drop", t.drop)
            editor.putString("target $i material", t.material)
        }
        editor.apply()
    }
    fun resumeGame(){
        val sharedPref = (context as MainActivity).getPreferences(Context.MODE_PRIVATE)
        lives = sharedPref.getInt("lives", 3)
        score = sharedPref.getInt("score", 0)
        val nr = sharedPref.getInt("number", 0)
        if(nr != 0) {
            targetList = ArrayList<Target>()
            for (i in (0..nr)) {
                val x = sharedPref.getInt("target $i x", 0)
                val y = sharedPref.getInt("target $i y", 0)
                val strength = sharedPref.getInt("target $i strength", 0)
                val drop = sharedPref.getString("target $i drop", "empty")
                val material= sharedPref.getString("target $i material", "glass")
                targetList!!.add(Target(x, y, 0F, 0F, 0F, 0F, strength, drop, material))
            }
        }

    }


    fun update() {
        if(ballY == -1f) return

        ballX+=dx
        ballY+=dy
        if(!wasFirstDraw)return


        if (ballX <= 0 || ballX+ballSize >= width || isBallTouchPaddleSide()) {
            dx = -dx
        }
        if (ballY <= 0 || isBallTouchPaddle()) {
            dy = -dy
        }
        for(t : Target in targetList!!){
            isBallTouchTarget(t)
        }
        if (ballY + ballSize >= height){
            Log.d("serca", "a tutaj wchodzi?")
            removeLives()
        }
    }

    private fun removeLives() {
        lives --
        dy = 5f
        dx = 5f
        paddleSize = 150f

        Log.d("serca", "usunięcie $lives")
        if(lives == 0){
            /**
            Log.d("serca", "powinno wejść")
            val alertDialog = AlertDialog.Builder(context).create()
            alertDialog.setTitle("Game over")
            alertDialog.setMessage("Your score is: $score")
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "New Game"
            ) { dialog, _ -> dialog.dismiss() }
            alertDialog.show()
            */
            targetList!!.clear()
            messageShow("Game Over")
            messageShow("Your score is $score")
            createTargetMap(5,9)
            lives = 3
            score = 0
            backUpGame()
        }
        ballX = 30f
        ballY = -1f
        if(ballY == -1f){
            ballY = paddleTop - 180f
        }
    }
    @SuppressLint("ShowToast")
    fun messageShow(message: CharSequence) {
        (context as MainActivity).runOnUiThread {
            run {
                var toast: Toast = Toast.makeText(context, message, Toast.LENGTH_SHORT)
                toast.setGravity(Gravity.CENTER, 0, 0)
                toast.show()
            }
        }
    }

    private fun isBallTouchPaddle() : Boolean{
        val bottom = arrayOf(ballX + ballSize/2, ballY + ballSize)
        val tmpT : Target  = Target(0,  0, paddleX, paddleTop - 30f, paddleSize, 30f, 0, "", "")
        if(pointIsInTarget(tmpT, bottom)) return true
        return false
    }
    private fun isBallTouchPaddleSide() : Boolean{
        val lSide = arrayOf(ballX, ballY + ballSize/2)
        val rSide = arrayOf(ballX + ballSize, ballY + ballSize/2)
        val tmpT : Target  = Target(0,  0, paddleX, paddleTop  - 30f, paddleSize, 30f, 0, "", "")
        if(pointIsInTarget(tmpT, rSide) || pointIsInTarget(tmpT, lSide)) return true
        return false
    }
    private fun isBallTouchTarget(t : Target){
        var s = t.strength
        val lSide = arrayOf(ballX, ballY + ballSize/2)
        val rSide = arrayOf(ballX + ballSize, ballY + ballSize/2)
        val bottom = arrayOf(ballX + ballSize/2, ballY + ballSize)
        val top = arrayOf(ballX + ballSize/2, ballY)

        if (pointIsInTarget(t, lSide) || pointIsInTarget(t, rSide)) {
            dx = -dx
            s--
        }
        if (pointIsInTarget(t, top) || pointIsInTarget(t, bottom)) {
            dy = -dy
            if (s == t.strength) {
                s--
            }
        }
        if(s!=t.strength)
            Log.d("myDebug", "${t.x} ${t.y} ${t.xCoordinate} ${t.yCoordinate}, ${t.strength}, ${t.drop}, ${t.material}, $s")
        t.strength = s
        if(s == 0){
            score++
            if(t.material == "brick") score++
            if(t.material == "steel") score++
            Log.d("dropy", t.drop)
            when(t.drop){
                "heart" -> lives++
                "doubleSpeed" -> {
                    dy*=2
                    dx*=2
                }
                "undoubleSpeed" -> {
                    dy/=2
                    dx/=2
                    if(dy < 0.25){
                        dy = 0.25f
                        dx = 0.25f
                    }
                }
                "doubleSize" -> paddleSize *=2
                "undoubleSize" -> paddleSize /= 2
            }
            targetList!!.remove(t)
        }
        if(targetList!!.size == 0){
            messageShow("You Win, Your score is $score")
            restartGame()
        }
    }

    fun restartGame() {
        ballX = 30f
        ballY = -1f//todo
        dx = 0f
        dy = 0f
        ballSize = 30f
        paddleX = 0f
        paddleSize = 150f
        paddleTop = height -30f
        targetList = ArrayList<Target>()
        targetXNumber = 0
        targetYNumber = 0
        wasFirstDraw = false
        score = 0
        lives = 3
        createTargetMap(5,9)
        backUpGame()
    }

    private fun pointIsInTarget(t : Target,point : Array<Float>) : Boolean{
        if(point[1] >= t.yCoordinate && point[1] <= t.yCoordinate + t.height && point[0] >= t.xCoordinate && point[0] <= t.xCoordinate + t.width){
            return true
        }
        return false
    }


    @SuppressLint("CanvasSize")
    override fun draw(canvas: Canvas?) {
        super.draw(canvas)

        if (canvas == null) return
        wasFirstDraw = true

        val red = Paint()
        red.setARGB(255,255,0,0)
        val grey = Paint()
        grey.setARGB(255,128,128,128)
        val glass = Paint()
        glass.setARGB(255,0, 204,204)
        val glassList= arrayOf(glass)
        val brick = Paint()
        brick.setARGB(255,204, 102,0)
        val brick2 = Paint()
        brick2.setARGB(255,174, 72,0)
        val brickList = arrayOf(brick, brick2)

        val steel = Paint()
        steel.setARGB(255,160, 160,160)
        val steel2 = Paint()
        steel2.setARGB(255,130, 130,130)
        val steel3 = Paint()
        steel3.setARGB(255,100, 100,100)
        val steelList = arrayOf(steel, steel2, steel3)

        canvas.drawOval(RectF(ballX, ballY,ballX+ballSize,ballY+ballSize), red)
        paddleTop = canvas.height - 30f
        if(ballY == -1f){
            ballY = paddleTop - 180f
        }
        canvas.drawRect(paddleX, paddleTop, paddleX + paddleSize, canvas.height - 60f, grey )
        val tHeight = 30f
        val breakArea = 5f
        val begin = breakArea * 6
        val tWidth = (canvas.width - begin*2 - breakArea*(targetXNumber-1))/targetXNumber

        for(target : Target in targetList!!){
            if(target.xCoordinate == 0F){
                target.xCoordinate = begin + (target.x - 1) * (breakArea + tWidth)
                target.yCoordinate = begin + (target.y - 1) * (breakArea + tHeight)
                target.height = tHeight
                target.width = tWidth
            }
            var material : Array<Paint> = glassList
            if(target.material == "steel") material = steelList
            if(target.material == "brick") material = brickList
            canvas.drawRect(target.xCoordinate, target.yCoordinate, target.xCoordinate + tWidth, target.yCoordinate + tHeight, material[target.strength - 1])
        }

    }
    override fun onTouchEvent(event: MotionEvent): Boolean {
        if(dx == 0f){
            dx = 5f
            dy = 5f
        }

        paddleX = event.x - (paddleSize)
        if (paddleX <= 0) paddleX = abs(dx)
        if (paddleX+paddleSize >= width) paddleX = width.toFloat() - paddleSize - abs(dx)

        return true

        return super.onTouchEvent(event)
    }
/**

    fun saveScore() {
        try {
            val fileOut = (context as MainActivity).openFileOutput("file", Context.MODE_PRIVATE)
            val objectOut = (context as MainActivity).ObjectOutputStream(fileOut)
            objectOut.writeObject(score)
            objectOut.close()
            Log.d("myDebug", "The Object  was succesfully written to a file")

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    fun getState() {
        try {
            val fileIn = openFileInput("file")

            val objectIn = ObjectInputStream(fileIn)
            tasklist = objectIn.readObject() as ArrayList<Task>
            sortTasklist()
            Log.d("stanList", " po sorcie")
            logList()
            objectIn.close()

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        if(myadapter != null) myadapter!!.notifyDataSetChanged()
    }
    */

}